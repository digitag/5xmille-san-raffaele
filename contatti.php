<!doctype html>
<html>

<head>
    <?php include( 'layout/head.php'); ?>
    <title>5XMILLE Contatti Ospedale San Raffaele di Milano</title>
<meta name="description" content="Contatti Ospedale San Raffaele di Milano">
<meta name="keywords" content="san raffaele milano, ospedale san raffaele, ospedale san raffaele milano, 5 x mille san raffaele">
</head>

<body>
<script type='text/javascript'>
var ebRand = Math.random()+'';
ebRand = ebRand * 1000000;
//<![CDATA[ 
document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468748&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
//]]>
</script>
<noscript>
<img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468748&amp;ns=1"/>
</noscript>
<?php include( 'layout/header.php'); ?>


    <div class="container">
        <h1>Contatti</h1>
        <p>Per maggiori informazioni sul <strong>5x1000 all'Ospedale San Raffaele di Milano</strong>:</p>
        <p><strong>Tel</strong>: 02 2643 4461
            <br />
            <strong>Fax</strong>: 02 2341 7386
            <br />
            <strong>E-mail</strong>: <a href="mailto:5xmille@hsr.it">5xmille@hsr.it</a>
        </p>
        <h3>Compila il form sottostante, saremo lieti di risponderti.</h3>
        <div class="row">

            <div class="col-md-6">
                <form id="contacts-form" onsubmit="mmConversionTag(465710, this);sendMailContatti();return false;">
                    <div class="form-group">
                        <label>Nome e Cognome</label>
                        <input type="text" class="form-control" name="name" placeholder="Nome e Cognome">
                    </div>
                    <div class="form-group">
                        <label>Indirizzo email</label>
                        <input type="email" class="form-control" name="email" placeholder="Indirizzo email">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" name="message" placeholder="Il tuo messaggio"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="privacy"> Accetto l'
                            <button class="btn-info" type="button" class="btn" data-toggle="modal" data-target="#myModal">informativa sulla privacy</button>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default red-button-small">INVIA</button>
                </form>
            </div>
            <div class="col-md-6 contacts-feedback">
                <p class="success">Messaggio inviato con successo, grazie.</p>
                <p class="error">Si è verificato un errore. Inserisci nuovamente i tuoi dati, grazie.</p>
            </div>

        </div>
    </div>


    <?php include( 'layout/footer.php'); ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">INFORMATIVA PRIVACY PER REGISTRAZIONI.</h4>
                </div>
                <div class="modal-body">
                    <p>Ai sensi dell'art.13 del D.Lgs.196/03, i suoi dati anagrafici saranno raccolti da Ospedale San Raffaele s.r.l. (Titolare del Trattamento dei Dati) per inviarle newsletter e comunicazioni sulle attività svolte dallo stesso e dalla Fondazione Centro San Raffaele per quanto riguarda la ricerca scientifica. I dati raccolti saranno registrati e custoditi in un database informatico e comunicati a terzi incaricati da Ospedale San Raffaele di Milano per l’adempimento di operazioni di spedizione, ma non trasferiti all'estero né diffusi. Il conferimento dei Suoi dati è facoltativo ma il rifiuto puó comportare l’impossibilità del perseguimento delle finalità sopra indicate. Qualora Lei consenta il trattamento dei Suoi dati, questi saranno gestiti e custoditi con le misure di sicurezza previste dalla legge per evitarne la perdita, la distruzione e l’utilizzo illecito. In conformità all’art. 7 del D.Lgs 196/03 è possibile chiedere la variazione, l’integrazione ed anche l’eventuale cancellazione a seguito di espressa richiesta scritta contattando il Titolare o la Direzione Marketing e Comunicazione (Responsabile del Trattamento dei dati personali), via Olgettina 58, Milano, tel. 02.2643.4461, e-mail: newsletter@hsr.it.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="js/main.js"></script>
</body>

</html>