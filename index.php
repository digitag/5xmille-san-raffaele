<!doctype html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <?php include( 'layout/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <title>5xMILLE Ospedale San Raffaele di Milano</title>
    <meta name="description" content="Devolvi il tuo 5XMILLE all'Ospedale San Raffaele. Non c'è cura, senza ricerca">
    <meta name="keywords" content="5xmille, 5x1000, 5XMILLE, 5 x mille ricerca, 5 x mille san raffaele">
</head>

<body>
    <script type='text/javascript'>
        var ebRand = Math.random() + '';
        ebRand = ebRand * 1000000;
        //<![CDATA[ 
        document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=470106&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=470106&amp;ns=1" />
    </noscript>


    <?php include( 'layout/header.php'); ?>

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="4000">
        <div id="freccia"><img class="animated bounce" src="img/freccia.png" width="37" height="37">
            <p>Scopri come darci il tuo 5xMILLE</p>
        </div>

        <div class="carousel-inner" role="listbox">
            <div class="item active split_main" id="slide1"></div>
            <div class="item split_main" id="slide2"></div>
            <div class="item split_main" id="slide3"></div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="container" id="infografica-1">
        <h1>Devolvi il tuo 5XMILLE all'Ospedale San Raffaele di Milano</h1>
        <div class="row">
            <div class="col-md-6">
                <p><span class="numero yellow">1</span> Nel modulo per la dichiarazione dei redditi puoi scegliere a chi destinare il 5 per mille dell’IRPEF.</p>
                <div class="clearfix"></div>
                <p><span class="numero red">2</span> Indica il codice fiscale dell'Ospedale San Raffaele di Milano e apponi la firma alla voce <em>Finanziamento della Ricerca Sanitaria</em>.</p>
                <div class="clearfix"></div>
                <p><span class="numero blue">3</span> Destinare il 5 per mille all’Ospedale San Raffaele di Milano significa sostenere la ricerca scientifica di eccellenza.</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <a class="red-button scopri" href="5x1000.php">Scopri come fare</a>
        <div class="pull-right visible-lg visible-sm" id="freccia2"><img class="animated bounce" src="img/freccia.png" width="37" height="37">
        </div>
    </div>


    <div id="dona-ora" class="container-fluid yellow yellow-band">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>Puoi sostenere la ricerca anche con una donazione</h2>
                    <ul>
                        <li>Conto corrente postale n. 1012856397</li>
                        <li>Bonifico bancario IBAN: IT03 U020 0809 4320 0010 1974 276</li>
                        <li>Carta di credito <a href="http://register.hsr.it/donazioni/donazione.aspx" target="_blank">register.hsr.it/donazioni/donazione.aspx</a> </li>
                    </ul>
                    <p>Intestazione: OSPEDALE SAN RAFFAELE SRL
                        <br> Causale: «Sostegno alla ricerca» </p>
                </div>
                <div class="col-md-4">
                    <a class="red-button" href="http://register.hsr.it/donazioni/donazione.aspx" target="_blank">Dona ora</a>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row" id="ricerca">
            <div class="col-md-8">
                <div id="dati-ricerca">
                    <h2>La ricerca al San Raffaele</h2>
                    <img class="pull-left" src="img/ricercatori.jpg" width="50" height="50" alt="ricercatori">
                    <p><span>1.683</span> RICERCATORI, MEDICI E TECNICI</p>
                    <div class="clearfix"></div>

                    <img class="pull-left" src="img/laboratori.jpg" width="50" height="50" alt="laboratori">
                    <p>Oltre <span>150</span> LABORATORI</p>
                    <div class="clearfix"></div>

                    <img class="pull-left" src="img/pubblicazioni.jpg" width="50" height="50" alt="pubblicazioni scientifiche">
                    <p><span>1.179</span> PUBBLICAZIONI SCIENTIFICHE</p>


                </div>
            </div>
            <div class="col-md-4">
                <a class="red-button ricerca-button" href="la-ricerca-ospedale-san-raffaele.php">Scopri i numeri della ricerca</a>
            </div>
            <div class="pull-right visible-lg visible-sm" id="freccia3"><img class="animated bounce" src="img/freccia.png" width="37" height="37">
            </div>
        </div>
    </div>



    <div class="container-fluid yellow yellow-band" id="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>Newsletter “Salute & Ricerca”</h2>
                    <p>Vuoi ricevere consigli di salute, aggiornamenti sulla ricerca e rimanere sempre informato sulle attività dell’Ospedale San Raffaele di Milano?
                        <br> Iscriviti alla Newsletter!</p>
                </div>
                <div class="col-md-4">
                    <a class="red-button" href="http://register.hsr.it/sr/register.aspx?from=5x1000" target="_blank">Iscriviti</a>
                </div>
            </div>
        </div>
    </div>


    <?php include( 'layout/footer.php'); ?>

    <script src="js/main.js"></script>
</body>

</html>