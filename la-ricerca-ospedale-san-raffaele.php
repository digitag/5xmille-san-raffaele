<!doctype html>
<html>

<head>
    <?php include( 'layout/head.php'); ?>
    <title>5xMILLE La Ricerca dell'Ospedale San Raffaele di Milano</title>
    <link rel="stylesheet" type="text/css" href="css/rendicontazione.css">
    <meta name="description" content="La Ricerca all'Ospedale San Raffaele. Non c'è cura, senza ricerca">
    <meta name="keywords" content="san raffaele milano, ospedale san raffaele, ospedale san raffaele milano, 5 x mille san raffaele">
</head>

<body>
    <script type='text/javascript'>
        var ebRand = Math.random() + '';
        ebRand = ebRand * 1000000;
        //<![CDATA[ 
        document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468745&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468745&amp;ns=1" />
    </noscript>
    <?php include( 'layout/header.php'); ?>


    <div class="container">
        <h1>La Ricerca dell'Ospedale San Raffaele di Milano</h1>
        <p>La ricerca del San Raffaele si caratterizza come <strong>innovativa e all'avanguardia</strong>. La sua missione è di favorire la progressione delle conoscenze sulle malattie umane, identificare nuove terapie a beneficio di pazienti e creare l’ambiente più favorevole alla crescita di giovani scienziati e giovani medici.</p>

        <h2>Le aree di ricerca</h2>
        <img class="img-responsive hidden-xs" src="img/la-ricerca.jpg" width="1170" height="400" alt="La ricerca del San Raffaele">
        <img class="img-responsive visible-xs" src="img/la-ricerca-xs.jpg" width="406" height="400" alt="La ricerca del San Raffaele">
        <p class="red-title bigger">Malattie cardiovascolari</p>
        <p><strong><em>Impiantata per la prima volta al San Raffaele la protesi valvolare che non necessita di punti di sutura</em></strong>
        </p>
        <p>È stata impiantata per la prima volta, su un paziente di 74 anni, la valvola aortica Perceval, una protesi valvolare auto ancorante, che non necessita quindi di punti di sutura.</p>
        <p>Questa protesi può essere impiantata nei pazienti affetti da stenosi aortica, la malattia cardiaca valvolare acquisita più comune nel mondo occidentale e la cui insorgenza aumenta con l’invecchiamento della popolazione. La prognosi dei pazienti affetti da stenosi aortica sintomatica grave è poco favorevole, se il paziente non viene sottoposto a trattamento subito dopo l’insorgenza dei sintomi.</p>

        <p>Oggi è possibile sostituire la valvola danneggiata con la protesi che può essere posizionata e ancorata nel sito d’impianto senza l’ausilio dei punti di sutura, grazie a un suo esclusivo dispositivo di ancoraggio autoespandibile. Se prima per una sostituzione valvolare la procedura di fissaggio richiedeva 30-40 minuti, oggi in 10 minuti è possibile inserire la valvola con un’importante riduzione dei tempi operatori che vanno a vantaggio del paziente.</p>

        <p class="red-title bigger">Diabete</p>
        <p><strong><em>Nao: il robot che aiuta i bambini affetti da diabete</em></strong>
        </p>
        <p>Il progetto Aliz-E (strategie adattive per interazioni sociali sostenibili nel lungo termine) è un progetto di robotica cognitiva che utilizza un robot semi-autonomo per instaurare una relazione personalizzata con bambini tra i 7 ed i 13 anni con Diabete di Tipo I e aiutarli a gestire al meglio la loro patologia. </p>

        <p>Grazie a una memoria interna, il robot è in grado di interagire e dialogare con i bambini, di muoversi e ballare con loro, di chiamarli per nome esprimendo emozioni. Grazie a queste caratteristiche, Nao li aiuta, attraverso il gioco, ad apprendere importanti concetti legati al diabete e a capire l’importanza di una sua corretta gestione. </p>


        <p class="red-title bigger">Malattie neurodegenerative</p>
        <p><em><strong>Individuato un nuovo fattore per la diagnosi della malattia di Alzheimer</strong></em>
        </p>
        <p>Un’équipe di medici e ricercatori dell’Istituto di Neurologia Sperimentale dell’Ospedale San Raffaele di Milano hanno individuato un nuovo fattore per la diagnosi e la prognosi della malattia di Alzheimer.</p>

        <p>La malattia inizialmente colpisce l’ippocampo, sede della memoria a breve termine, per poi diffondere al resto del cervello. Come avvenga questa progressione non è del tutto noto. I ricercatori del San Raffaele hanno scoperto che tale diffusione potrebbe avvenire attraverso le fibre nervose che connettono le aree inizialmente colpite alle altre regioni cerebrali.</p>

        <p>Lo studio ha coinvolto 106 pazienti affetti da malattia di Alzheimer in stadio già avanzato e 51 pazienti che presentavano i primi sintomi della malattia.</p>


        <p class="red-title bigger">Malattie genetiche</p>
        <p><em><strong>Sperimentato con successo il trapianto di midollo per la cura della neuromielite ottica</strong></em>
        </p>
        <p>Un team multidisciplinare dell’Ospedale San Raffaele di Milano ha effettuato per la prima volta al mondo e con risultati molto promettenti il trapianto di cellule staminali ematopoietiche da donatore allogenico, in due pazienti affetti da forme particolarmente aggressive di neuromielite ottica, una malattia infiammatoria del sistema nervoso molto grave e fortemente invalidante.
            <br> Implacabile nei suoi attacchi e nella sua progressione, nel giro di pochi anni compromette il nervo ottico e quindi la vista, e la capacità di movimento, fino alla paresi.</p>

        <p>Finora non aveva una terapia efficace, ma oggi c’è una speranza concreta di poterne bloccare l’evoluzione grazie al trapianto di midollo osseo da donatore.
            <br> I due pazienti trattati, un uomo con gravi difficoltà di movimento, camminava solo con l’aiuto di un sostegno, e una donna costretta a letto, si erano entrambi sottoposti a trapianto di midollo autologo, ossia con cellule progenitrici del sangue prelevato dallo stesso paziente, ma non aveva funzionato. Il trapianto da donatore (allogenico) ha funzionato: a 3 anni dall’intervento, entrambi i pazienti stanno bene, la malattia non è ricomparsa.</p>


        <p class="red-title bigger">Tumori</p>
        <p><em><strong>Tumori del sangue: scoperto il meccanismo per riattivare un gene sentinella che elimina le cellule impazzite</strong></em>
        </p>
        <p>L’équipe ha individuato un meccanismo attraverso il quale le cellule tumorali del sangue riescono a superare le barriere che si oppongono alla loro proliferazione indisturbata, causando malattie quali leucemie, mielomi e linfomi. Le cellule tumorali presentano una crescita tumultuosa e, come conseguenza, accumulano danni al DNA che, in una cellula sana, indurrebbero morte cellulare (apoptosi). Gli scienziati, indagando sul perché in caso di tumori del sangue questo non avvenga, hanno scoperto che le cellule tumorali ematologiche spengono un gene sentinella – YAP 1 - il cui compito è riconoscere la cellula “impazzita” e indurla ad apoptosi.</p>

        <p>I ricercatori hanno poi identificato una proteina – STK4 – responsabile dello spegnimento di YAP 1 e scoperto che l’inattivazione di questa proteina ripristina i livelli di YAP1, inducendo la morte di cellule tumorali ematologiche.</p>


        <p class="red-title bigger">AIDS e malattie infettive</p>

    </div>
    </div>


    <?php include( 'layout/footer.php'); ?>

    <script src="js/main.js"></script>
</body>

</html>