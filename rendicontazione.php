<!doctype html>
<html>

<head>
    <?php include( 'layout/head.php'); ?>
    <title>5XMILLE Rendicontazione Ospedale San Raffaele di Milano</title>
    <meta name="description" content="La Ricerca all'Ospedale San Raffaele. Cosa abbiamo fatto.">
    <meta name="keywords" content="san raffaele milano, ospedale san raffaele, ospedale san raffaele milano, 5 x mille san raffaele">
    <link rel="stylesheet" type="text/css" href="css/rendicontazione.css">
</head>

<body>
    <script type='text/javascript'>
        var ebRand = Math.random() + '';
        ebRand = ebRand * 1000000;
        //<![CDATA[ 
        document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468746&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468746&amp;ns=1" />
    </noscript>
    <?php include( 'layout/header.php'); ?>


    <div class="container" id="rendicontazione">
        <h1>Cosa abbiamo fatto</h1>
        <p>Come sono stati utilizzati i fondi derivanti dal 5 per mille.</p>

        <h2 class="blue-bg"><strong>Anno 2012</strong> - Fondi raccolti <span class="bigger">€ 4.341.347</span></h2>
        <div class="clearfix"></div>
        <p><span class="bigger">€ 1.500.000</span> Piattaforma Tecnologica di Genomica Traslazionale e Bioinformatica</p>
        <p><span class="bigger">€ 1.450.000</span> Piattaforma di medicina rigenerativa in ambito neurologico</p>
        <p><span class="bigger">€ 1.391.347</span> Piattaforma per il potenziamento della medicina traslazionale</p>

        <div id="anno2012">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="blue-bg"><strong>Anno 2011</strong> - Fondi raccolti <span class="bigger">€ 5.765.966</span></h2>
                    <div class="clearfix"></div>
                    <p class="red-title bigger">DIABETE</p>
                    <p><strong>€ 700.000</strong> Trapianto di isole pancreatiche nel paziente con diabete di Tipo I
                        <br>
                        <strong>€ 300.000</strong> Approcci terapeutici innovativi applicati alle malattie renali </p>

                    <p class="red-title bigger">TUMORI</p>
                    <p><strong>€ 180.000</strong> Adenocarcinoma pancreatico: sviluppo in preclinica di nuove terapie per il trattamento della malattia in stadio avanzato.
                        <br>
                        <strong>€ 300.000</strong> Prostata: integrazione di tecnologie avanzate per lo studio delle metastasi del carcinoma prostatico</p>

                    <p class="red-title bigger">MALATTIE CARDIOVASCOLARI</p>
                    <p><strong>€ 540.000</strong> Infarto – aneurisma: ricerca sperimentale e clinica </p>

                    <p class="red-title bigger">MALATTIE GENETICHE</p>
                    <p><strong>€ 250.000</strong> Ipotiroidismo: studio dei fattori genetici per personalizzare la terapia
                        <br>
                        <strong>€ 2.195.966</strong> Implementazione nello studio e nella ricerca per l’identificazione di geni e proteine responsabili di gravi malattie, attraverso l’uso della Biobanca (struttura per la conservazione di materiale biologico umano per finalità di diagnosi, studi e ricerca)
                    </p>
                    <p class="red-title bigger">RICERCHE CLINICHE</p>
                    <p><strong>€ 1.000.000</strong> Identificazione di un nuovo piano per le infezioni ospedaliere
                        <br>
                        <strong>€ 300.000</strong> Studio sulla fertilità in coppie europee caucasiche
                    </p>

                </div>
            </div>
        </div>


        <h2 class="blue-bg"><strong>Anno 2010</strong> - Fondi raccolti <span class="bigger">€ 6.594.756</span></h2>
        <div class="clearfix"></div>
        <p class="red-title bigger">CELLULE STAMINALI</p>
        <p><strong>€ 994.754</strong> Utilizzo delle cellule staminali per la medicina rigenerativa</p>

        <p class="red-title bigger">Centro di Genomica</p>
        <p><strong>€ 1.500.000</strong> Piattaforma tecnologica di Genomica traslazionale e Bioinformatica
            <br>
            <strong>€ 1.300.000</strong> Piattaforma tecnologica di proteomica e microsequenziamento delle proteine
        </p>

        <p class="red-title bigger">Centro di Imaging Sperimentale</p>
        <p><strong>€ 2.000.000</strong> Integrazione di tecnologie di Bioimaging in un’unica piattaforma tecnologica di Imaging sperimentale</p>

        <p class="red-title bigger">Centro Europeo di Modelli di malattia</p>
        <p><strong>€ 800.000</strong> Piattaforma tecnologica di sperimentazione pre-clinica per l’identificazione di bersagli molecolari di terapie farmacologiche innovative</p>

    </div>


    </div>

    <?php include( 'layout/footer.php'); ?>
    <script src="js/main.js"></script>
</body>

</html>