<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">

<meta property="og:title" content="IL TUO 5XMILLE ALL’OSPEDALE SAN RAFFAELE DI MILANO" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://www.5x1000.org/5xmille-san-raffaele.jpg" />
<meta property="og:description" content="Devolvi il tuo 5 per mille all’Ospedale San Raffaele di Milano, perché al centro della ricerca ci sei tu. NON C’È CURA, SENZA RICERCA. CODICE FISCALE 07636600962 NEL RIQUADRO “FINANZIAMENTO DELLA RICERCA SANITARIA" />
<link rel="image_src" type="image/jpeg" href="http://www.5x1000.org/5xmille-san-raffaele.jpg" />

<script src="http://ds.serving-sys.com/BurstingRes/CustomScripts/mmConversionTagV4.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>