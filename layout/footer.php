<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2>Restiamo in contatto</h2>
            <p><strong>Tel</strong>: 02 2643 4461
                <br />
                <strong>Fax</strong>: 02 2341 7386
                <br />
                <strong>E-mail</strong>: <a href="mailto:5xmille@hsr.it">5xmille@hsr.it</a>
            </p>
        </div>
        <div class="col-md-6">
            <h2>Hai scelto di darci il tuo 5XMILLE?</h2>
            <p>Mandaci i tuoi dati, saremo lieti di ringraziarti!</p>
            <form id="footer-form" action="#" onsubmit="mmConversionTag(607685, this);sendMail();return false;">
                <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" name="name" placeholder="Nome e Cognome">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" placeholder="Indirizzo email">
                        </div>
                        <div class="checkbox col-md-12">
                            <label>
                                <input type="checkbox" name="privacy"> Accetto l'
                                <button class="btn-info" type="button" class="btn" data-toggle="modal" data-target="#myModal">informativa sulla privacy</button>
                            </label>

                            <button type="submit" class="btn btn-default red-button-small pull-right">INVIA</button>
                        </div>
                        <div class="col-md-12 form-feedback">
                            <p class="success">Messaggio inviato con successo, grazie.</p>
                            <p class="error">Si è verificato un errore. Inserisci nuovamente i tuoi dati, grazie.</p>
                        </div>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="container" id="loghi">

    <a class="red-button download vcenter" href="doc/come-devolvere-il-5-per-mille-al-San-Raffaele-di-Milano.pdf" target="_blank">Scarica il<br />
promemoria</a>

<div id="fb" class="vcenter"> <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.5xmille.org/" target="_blank"><img src="img/facebook.png" width="76" height="76" alt="5xmille San Raffaele" /></a>
<p>Il mio 5 per mille all’Ospedale<br />
San Raffaele di Milano<br />
Codice Fiscale: 07636600962 <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.5xmille.org/" target="_blank">CONDIVIDI</a></p>
</div>
<a class="vcenter" href="http://www.hsr.it/" target="_blank"><img class="pull-left" src="img/ospedale-san-raffaele.jpg" width="175" height="120" alt="Ospedale San Raffaele di Milano" /></a>

    <a href="http://www.sandonato-gsd.it/" target="_blank"><img class="vcenter" src="img/GSD.png" width="175" height="120" alt="GSD" />
    </a>

</div>


<div class="container-fluid" id="footer">
    <p class="text-center small">Ospedale San Raffaele - Via Olgettina 60, 20132 Milano, Italia - Tel. 02 26 431
        <br /> Copyright Reserved ©. 2015, Ospedale San Raffaele - Milano - P.I. 07636600962 | <a href="doc/Informativa-Privacy-sito-OSR.pdf" target="_blank">Informativa Privacy</a>
    </p>
    <p class="text-center" style="font-size:0.7em">"Questo sito utilizza cookie di carattere analitico, usati in forma aggregata, con lo scopo esclusivo di raccogliere informazioni per analisi statistiche, per miglioramenti e per monitorare il corretto funzionamento. I cookie raccolti attraverso Google Analytics riguardano informazioni in forma anonima sul modo in cui i nostri visitatori arrivano al sito, quali sono le pagine più visitate, le modalità di fruizione e quanto tempo dura la loro permanenza nel sito. Per i servizi online i cookie sono utilizzati al fine dell'autenticazione utente e gestiti attraverso i servizi propri di autenticazione."</p>
</div>

<script src="js/bootstrap.min.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {

            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),

            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-12522560-4', 'auto');

    ga('send', 'pageview');
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">INFORMATIVA PRIVACY PER REGISTRAZIONI.</h4>
            </div>
            <div class="modal-body">
                <p>Ai sensi dell'art.13 del D.Lgs.196/03, i suoi dati anagrafici saranno raccolti da Ospedale San Raffaele s.r.l. (Titolare del Trattamento dei Dati) per inviarle newsletter e comunicazioni sulle attività svolte dallo stesso e dalla Fondazione Centro San Raffaele per quanto riguarda la ricerca scientifica. I dati raccolti saranno registrati e custoditi in un database informatico e comunicati a terzi incaricati da Ospedale San Raffaele di Milano per l’adempimento di operazioni di spedizione, ma non trasferiti all'estero né diffusi. Il conferimento dei Suoi dati è facoltativo ma il rifiuto puó comportare l’impossibilità del perseguimento delle finalità sopra indicate. Qualora Lei consenta il trattamento dei Suoi dati, questi saranno gestiti e custoditi con le misure di sicurezza previste dalla legge per evitarne la perdita, la distruzione e l’utilizzo illecito. In conformità all’art. 7 del D.Lgs 196/03 è possibile chiedere la variazione, l’integrazione ed anche l’eventuale cancellazione a seguito di espressa richiesta scritta contattando il Titolare o la Direzione Marketing e Comunicazione (Responsabile del Trattamento dei dati personali), via Olgettina 58, Milano, tel. 02.2643.4461, e-mail: newsletter@hsr.it.
                </p>
            </div>
        </div>
    </div>
</div>