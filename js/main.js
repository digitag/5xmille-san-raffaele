var endpoint = "https://whitestar.digitag.me/send-email";
function altezzaSplit() {
    $(".split_main").css('min-height', $(window).height() - 137);
}

function sendMailContatti() {
    loading();

    if ($('#contacts-form textarea[name=message]').val() == '') {
        alert('Inserisci il tuo messaggio');
        return false;
    }
    if ($('#contacts-form input[name=email]').val() == '') {
        alert('Inserisci un indirizzo email');
        return false;
    }
    if ($('#contacts-form input[name=privacy]').is(':checked') == false) {
        alert('Devi accettare l\'informativa sulla privacy');
        return false;
    }

    $('#contacts-form [type=submit]').html('SENDING')
    $.post(endpoint, $('#contacts-form').serialize(), function (data) {
        if (data.response != 'success') {
            alert(data.message);
            $('.contacts-feedback .error').show();
        } else {
            $('#contacts-form [type=submit]').html('INVIA');
            $('.contacts-feedback .success').show();
        }
        stopLoading();
    });
}

function sendMail() {
    loading();

    if ($('#footer-form input[name=email]').val().length == 0) {
        alert('Inserisci un indirizzo email');
        return false;
    }
    if ($('#footer-form input[name=privacy]').is(':checked') == false) {
        alert('Devi accettare l\'informativa sulla privacy');
        return false;
    }

    $('#footer-form [type=submit]').html('SENDING')
    $.post(endpoint, $('#footer-form').serialize(), function (data) {
        if (data.response != 'success') {
            alert(data.message);
            $('.form-feedback .error').show();
        } else {
            $('#footer-form [type=submit]').html('INVIA');
            $('.form-feedback .success').show();
        }
        stopLoading();
    });
}

function loading() {};

function stopLoading() {};

$(window).load(function () {
    altezzaSplit();
	$('#freccia').click(function(){
		$( "html, body" ).animate({
			scrollTop: $('#infografica-1').offset().top
			}, 1000);
		})
	$('#freccia2').click(function(){
		$( "html, body" ).animate({
			scrollTop: $('#dona-ora').offset().top
			}, 1000);
		})
	$('#freccia3').click(function(){
		$( "html, body" ).animate({
			scrollTop: $('#newsletter').offset().top
			}, 1000);
		})
});