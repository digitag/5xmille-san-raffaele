<!doctype html>
<html>

<head>
    <?php include( 'layout/head.php'); ?>
    <title>5XMILLE</title>
    <meta name="description" content="Devolvi il tuo 5XMILLE all'Ospedale San Raffaele. Non c'è cura, senza ricerca">
    <meta name="keywords" content="5xmille, 5x1000, 5XMILLE, 5 x mille ricerca, 5 x mille san raffaele">
</head>

<body>
    <script type='text/javascript'>
        var ebRand = Math.random() + '';
        ebRand = ebRand * 1000000;
        //<![CDATA[ 
        document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468744&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468744&amp;ns=1" />
    </noscript>
    <?php include( 'layout/header.php'); ?>


    <div class="container">
        <h1>Il 5XMILLE</h1>
        <h2>Cos'è</h2>
        <p>Il 5 per mille è una quota delle tasse che ogni contribuente, senza nessun costo, può destinare all’Ospedale San Raffaele di Milano e contribuire così al sostegno della ricerca sanitaria.</p>

        <h2>Destinare il 5 per mille al San Raffaele è semplice:</h2>
        <ul>
            <li>sul modulo per la dichiarazione dei redditi (CU-Certificazione Unica, ex CUD- 730, UNICO), individua la sezione <em>“Scelta per la destinazione del cinque per mille dell’IRPEF”</em>
            </li>
            <li>nel riquadro “Finanziamento della Ricerca Sanitaria” metti la tua firma e indica il nostro codice fiscale <strong><span class="big">07636600962</span></strong>
            </li>
        </ul>
        <img class="img-responsive" src="img/modello.jpg" width="1122" height="508" alt="modello 730 5xMille San Raffaele">
        <h2>Istruzioni per ogni modello</h2>
        <p> Consulta il modulo che corrisponde alla tua dichiarazione e scopri come è semplice destinare il 5 per mille. </p>
        <ul>
            <li><a href="doc/CU_2015_mod.pdf" target="_blank">CU</a>
            </li>
            <li><a href="doc/730_2015_modelli.pdf" target="_blank">Modello 730</a>
            </li>
            <li><a href="doc/UNICO_2015_mod.pdf" target="_blank">Modello Unico</a>
            </li>
        </ul>

        <h2>Chi non compila la dichiarazione dei redditi deve:</h2>
        <ul>
            <li>richiedere il modulo CU all’INPS o al datore di lavoro</li>
            <li>compilarlo nello spazio dedicato (come indicato sopra)</li>
            <li>inserirlo in una busta chiusa con la dicitura <em>"Scelta per la destinazione del cinque per mille dell'IRPEF”</em>, con il proprio cognome, nome e codice fiscale</li>
            <li>consegnarlo a uno sportello delle Poste o della banca, senza alcun costo</li>
        </ul>

        <h2>Il 5XMILLE non è l’8XMILLE</h2>
        <p>Scegliere di destinare il 5 per mille all’Ospedale San Raffaele di Milano non influisce sulla destinazione dell’8 per mille allo Stato oppure alla Chiesa.</p>
    </div>

    <?php include( 'layout/footer.php'); ?>

    <script src="js/main.js"></script>
</body>

</html>