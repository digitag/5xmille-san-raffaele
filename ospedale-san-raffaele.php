<!doctype html>
<html>

<head>
    <?php include( 'layout/head.php'); ?>
    <title>5xMILLE Ospedale San Raffaele di Milano</title>
    <meta name="description" content="La Ricerca all'Ospedale San Raffaele. Non c'è cura, senza ricerca">
    <meta name="keywords" content="san raffaele milano, ospedale san raffaele, ospedale san raffaele milano, 5 x mille san raffaele">
</head>

<body>
    <script type='text/javascript'>
        var ebRand = Math.random() + '';
        ebRand = ebRand * 1000000;
        //<![CDATA[ 
        document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468749&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
        //]]>
    </script>
    <noscript>
        <img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=468749&amp;ns=1" />
    </noscript>
    <?php include( 'layout/header.php'); ?>

    <div class="container">
        <h1>L'Ospedale San Raffaele di Milano</h1>
        <h2>Chi siamo</h2>
        <p>L’Ospedale San Raffaele di Milano è un policlinico universitario di rilievo internazionale e di alta specializzazione, riconosciuto “Istituto di Ricovero e Cura a Carattere Scientifico” (IRCCS) dal 1972.</p>
        <p>È centro di eccellenza per la cura di numerose patologie grazie all’interazione tra area clinica e ricerca scientifica, che facilita il trasferimento dei risultati dal laboratorio al letto del paziente.</p>
        <h2>I numeri dell'Ospedale San Raffaele di Milano</h2>
        <img class="img-responsive hidden-xs" style="margin:40px 0" src="img/infografica-numeri.jpg" width="1070" height="540" alt="I numeri dell'Ospedale San Raffaele di Milano">
        <div class="row">
            <div class="col-md-6 visible-xs">
                <p>Oltre <strong>6.000</strong> addetti
                    <br>
                    <strong>1.325</strong> posti letto
                    <br>
                    <strong>32</strong> sale operatorie
                    <br>
                    <strong>45.668</strong> ricoveri ordinari</p>

            </div>
            <div class="col-md-6 visible-xs">
                <p>
                    <strong>3.281.901</strong> esami di laboratorio
                    <br>
                    <strong>63.579</strong> accessi in Pronto Soccorso
                    <br>
                    <strong>5.272</strong> ricoveri Day Hospital e Day Surgery
                    <br>
                    <strong>894.274</strong> prestazioni ambulatoriali </p>
            </div>

        </div>
        <p style="margin-top:50px"><em>Ultimi dati disponibili al 31.12.2014</em>
        </p>
    </div>



    </div>

    <?php include( 'layout/footer.php'); ?>

    <script src="js/main.js"></script>
</body>

</html>